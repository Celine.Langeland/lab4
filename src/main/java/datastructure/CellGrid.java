package datastructure;

import java.util.ArrayList;
import java.util.List;

import cellular.CellState;

public class CellGrid implements IGrid {

    private List<CellState> cells;
    private int rows;
    private int columns;

    
    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.columns = columns;
        cells = new ArrayList <CellState> (columns*rows);
        for (int i = 0; i < rows * columns; i++) {
            cells.add(initialState);
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        return null;
    }
    
}
